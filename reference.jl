### A Pluto.jl notebook ###
# v0.14.3

using Markdown
using InteractiveUtils

# ╔═╡ ed0ac00f-d114-4901-a32f-f18443fd570d
using SCAM

# ╔═╡ 84bca88e-a38d-11eb-0ff6-836548bdba76
md"""# SCAM Reference"""

# ╔═╡ 91b01189-fcd5-4cb0-a485-cb6e70f9b63b
md"""## ==

`A == B` -- test for mathematical equality of A and B.  (May fail to evaluate to a boolean.)
"""

# ╔═╡ f851e751-865c-4f84-8ea9-6d44e39a5a7b
@s 1+1==2

# ╔═╡ 5433165f-f7dd-4271-afdf-73e3db3494be
@s a==b

# ╔═╡ d3282d94-e2c4-4b87-bd97-2bb7ab08d352
@s assume{a==b,a==1,b==2-c,c==1}

# ╔═╡ aa55e75b-ad3f-4723-8a98-52b13811051f
md"""## ===

`A === B` -- test for structural equality of A and B.  A and B are strictly evaluated first.

(Always succeeds in evaluating to a boolean)"""

# ╔═╡ 17553869-856a-495e-aed5-14a509416fbe
@s 1+1===2 # true because 1+1 simplifies to 2.

# ╔═╡ 45b8fd02-66df-46d5-bc9f-407f2c257ff8
@s a===b # a is different to b

# ╔═╡ 747287a5-63e0-4e45-a1d6-1713d95651b6
@s assume{a===b,a==b} # even under the assumption that a and b are mathematically equal, they still differ structurally.

# ╔═╡ 125a7203-2c9e-4185-a6d2-f409c292bd08
md"""## assume

`assume{F,a1,a2...}` -- evaluate `F` assuming the truth of `a1`,`a2`,...
"""

# ╔═╡ ef7ce068-33c7-4911-887c-d06394e3a7fc
@s assume{x^2>3,x>y,y>2} # SCAM can prove that x^2>3 if it knows that x>y and y>2.

# ╔═╡ 55de8d48-679b-4b1b-8b4d-4a54afd5e278
md"""## bag

`bag(ELEMENTS...)` -- an unordered collection of expressions with repetition.  The elements will be sorted into a canonical ordering.
"""

# ╔═╡ a368139b-3e69-4bf6-a38f-33ed4b83d4df
@s bag(1,2,3,2)

# ╔═╡ d0ba6deb-b805-4bdb-9029-98ba2746b22f
md"""## context

`context{F,var1,val1,var2,val2,...}` -- evaluate `F` in a subcontext where context variables `var1`,`var2`,... are set to values `val1`,`val2`,...

Subcontexts nest.
"""

# ╔═╡ 488621b2-4fcf-4e30-8a36-c0e8aefa3816
md"""## contextvalue

`contextvalue{V}` is the value of the context variable `V` in the current context.
"""

# ╔═╡ 92a4d6fa-ba23-4448-acbb-2230c97311fd
@s contextvalue{myvariable}

# ╔═╡ 95ce4fe1-553d-439b-9a0e-bafa8e78249c
@s context{contextvalue{myvariable},myvariable,5}

# ╔═╡ a0363900-088a-4755-a794-1ea081336ba7
@s contextvalue{integration_effort}

# ╔═╡ 51ee7ced-4f89-4406-99dc-209b5577e2fc
md"""## denominator

`denominator(x)` -- the denominator of rational `x`.
"""

# ╔═╡ 32bec8a9-72ad-438e-b07b-6ff6dfcf5f00
md"""## expand

`expand(F)` -- expand products and powers over `F`.

`expand{x,y,...}(F)` -- expand products and powers involving the variables x,y,... over `F`.
"""

# ╔═╡ f0bb31bf-6dbe-4070-b264-745d8db34761
@s expand(3*(1+a+x+y)*x*(1+a)^2)

# ╔═╡ 32a6b7b6-c63d-4057-9be2-1dda3f4dac02
@s expand{x,y}(3*(1+a+x+y)*x*(1+a)^2)

# ╔═╡ dbae6e00-e696-48d2-8db5-0ead90a73b7f
md"""## groebner

`groebner(F1,F2,...)` -- compute a Groebner basis for the polynomials `F1`,`F2`,... over tagged variables 𝗫.  (As might be built using polyFreeze.)

At present the basis is computed with respect to a grevlex order.
"""

# ╔═╡ abd1e2a2-323b-451d-a61d-8e7f10d653f7
@s groebner(polyFreeze(sin(x)^2+cos(x)^2-1),polyFreeze(sin(x)))

# ╔═╡ af7c1cc6-3e70-4402-9f5e-3364cd1aee9b
@s groebner(polyFreeze(sin(x)^2+cos(x)^2-1),polyFreeze(sin(x)),polyFreeze(cos(x)-1/2)) # We see that given the well known identity sin^2+cos^2==1, we cannot simultaneously have sin(x)=0 and cos(x)=1/2

# ╔═╡ ee5280b0-93ea-4026-bee2-0671609e2cff
md"""## IF

`IF(BOOLEAN,A,B)` -- if `BOOLEAN` is true then evaluate to `A` else evaluate to `B`
"""

# ╔═╡ 33105ca3-8784-469d-aef5-779cb999a656
@s IF(1+1==2,yes,no)

# ╔═╡ 631d714f-e096-4a92-92cb-728cd9edd7ef
md"""## isContinuous

`isContinuous{x}(F)` -- Viewed as a function of x, F is continuous at all x.
"""

# ╔═╡ 5ff923fe-1956-4d2a-9576-f87d0c15b418
@s isContinuous{x}(1/x^2) # Can't prove that 1/x^2 is continous for all x.

# ╔═╡ f309fd5d-098d-486a-befb-2f6c40402758
@s assume{isContinuous{x}(1/x^2),¬(x==0)} # 1/x^2 is continous away from x==0.

# ╔═╡ 99e4211e-4db7-4d15-9829-185c107c32a0
md"""## isFinite

`isFinite(X)` -- is `X` a finite number?
"""

# ╔═╡ 3347a4a4-8c33-4f57-b4f9-0c2e76523781
md"""## isFree

`isFree{F,x}` -- is `F` free of occurrences of `x`?
"""

# ╔═╡ e4af5e30-7fbb-4309-8c6b-541791cc1736
md"""## isGeneric

`isGeneric(x)` -- The variable `x` takes on generic values.

`isGeneric(F,x)` -- Does `F` vary generically if `x` is considered to vary generically?
"""

# ╔═╡ a677ce4b-fff7-49b4-a44d-9a0a9b973c4a
@s isGeneric(x^2,x) # x^2 varies generically if x does.

# ╔═╡ fad8fb11-79cd-48e3-bf90-dc7e77935ded
@s assume{x^2==0,isGeneric(x)} # For generic values of x, x^2 is not zero.

# ╔═╡ 497ea09b-9293-493d-910c-622e03213948
md"""## isInteger

`isInteger(X)` -- is `X` an integer?
"""

# ╔═╡ d592b362-4aa6-4905-a118-3410ebb7b9b1
md"""## isPolynomial

`isPolynomial{x}(F)` -- is `F` a polynomial over `x`?
"""

# ╔═╡ b703e868-7bb3-4a3b-9c08-9515ce19448c
@s isPolynomial{x}(sin(y)+(x^2+1)*(x-2))

# ╔═╡ 445f819e-1b85-40ab-baa0-a4bae62ea571
@s isPolynomial{y}(sin(y)+(x^2+1)*(x-2))

# ╔═╡ 74600acc-6871-4527-824e-5bf0dae8748a
md"""## mapcoeff

`mapcoeff{x,y,...}(F)` -- generates a λ form allowing a function to be mapped over the coefficients of terms involving the variables `x`,`y`,... in `F`.

*Currently this does not work quite as it should.  Note that the term $x^2$ in the following example should have an implicit coefficient of 1.*
"""

# ╔═╡ b2ca038c-d6f7-4032-8f53-da0d4021c6cc
@s mapcoeff{x,y}(a*sin(x)*cos(y)+x^2+2*x*y+a*y^2)

# ╔═╡ 94f6f3e5-adae-478c-84ba-bad33d7abf24
md"""## numerator

`numerator(x)` -- the numerator of rational `x`.
"""

# ╔═╡ 43038b33-81dc-45c0-a67f-9c9b75c04ef8
md"""## polyFreeze

`polyFreeze(F)` -- reinterpret `F` as a polynomial over variables 𝗫 with tags representing the non-polynomial or non-rational parts of the expression `F`.

The main reason for doing this is for use with the `groebner` form.
"""

# ╔═╡ a1ffb564-6bee-4535-a9c1-a7574c0739fe
@s polyFreeze(x^2+3*sin(y)+x*y+2)

# ╔═╡ 9ae65ab6-2fe4-48f3-bf86-6f41c0791c7e
md"""## polyThaw

Undo the effect of `polyFreeze`."""

# ╔═╡ aaa0e3b0-66db-4331-a7fe-0faf7c8d5800
@s polyThaw(2 + 𝗫{x} ^ 2 + 𝗫{x} * 𝗫{y} + 3 * 𝗫{sin(y)})

# ╔═╡ 13a539fb-4637-4da4-a901-ac6dda1358ce
md"""## subst

`subst{x,Y}{F}` substitute `Y` for `x` in `F`.
"""

# ╔═╡ c463c322-01ce-4d03-b3c7-31097919eb4b
md"""# λ

`λ{F,X...}` -- Anonymous function defined by `F` over the variables `X...`.
"""

# ╔═╡ 6036b1fc-0086-4e34-88cf-0d6423e418af
@s λ{x+2*y,x,y}(1,2)

# ╔═╡ d7b59463-b2a8-48ad-856c-8c148e19f3c2
md"""## ∂

`∂{F,x}` -- Differentiate `F` with respect to the variable `x`.
"""

# ╔═╡ f890ce1f-a252-4e05-b928-10cb67cc4a04
@s ∂{sin(x^2),x}

# ╔═╡ 3d83c00c-f7a0-415c-8675-df73a8096ae4
md"""## ∫

`∫{F,x}` -- attempt to find an antiderivative of `F` with respect to `x`.

`∫{F,x,a,b}` -- attempt to compute the definite integral of `F` with respect to `x` between the limits `a` and `b`.
"""

# ╔═╡ f3285fce-3d2f-4a43-84b0-91b433fa22e4
@s ∫{1/√(x^2-2),x}

# ╔═╡ 61e5718c-39c2-45c6-9cbb-c439fb0a5c67
@s ∫{√(1-x^2),x,0,1}

# ╔═╡ 25ea2cee-ba16-4da4-b789-6dcadfa6485c
md"""## 𝔻

`𝔻{f,n}` partial derivative of the function `f` with respect to the `n`th argument.
"""

# ╔═╡ 3219def9-6c0d-47a3-8cde-5e708be2bedf
@s ∂{f(x^2),x}

# ╔═╡ 356e89b9-c098-4bca-ba08-2bae5634170c
@s 𝔻{sin,1}

# ╔═╡ 37e4163e-34ef-4695-9094-8f272d533448
@s 𝔻{cos,1}

# ╔═╡ Cell order:
# ╟─ed0ac00f-d114-4901-a32f-f18443fd570d
# ╟─84bca88e-a38d-11eb-0ff6-836548bdba76
# ╟─91b01189-fcd5-4cb0-a485-cb6e70f9b63b
# ╠═f851e751-865c-4f84-8ea9-6d44e39a5a7b
# ╠═5433165f-f7dd-4271-afdf-73e3db3494be
# ╠═d3282d94-e2c4-4b87-bd97-2bb7ab08d352
# ╟─aa55e75b-ad3f-4723-8a98-52b13811051f
# ╠═17553869-856a-495e-aed5-14a509416fbe
# ╠═45b8fd02-66df-46d5-bc9f-407f2c257ff8
# ╠═747287a5-63e0-4e45-a1d6-1713d95651b6
# ╟─125a7203-2c9e-4185-a6d2-f409c292bd08
# ╠═ef7ce068-33c7-4911-887c-d06394e3a7fc
# ╟─55de8d48-679b-4b1b-8b4d-4a54afd5e278
# ╠═a368139b-3e69-4bf6-a38f-33ed4b83d4df
# ╟─d0ba6deb-b805-4bdb-9029-98ba2746b22f
# ╟─488621b2-4fcf-4e30-8a36-c0e8aefa3816
# ╠═92a4d6fa-ba23-4448-acbb-2230c97311fd
# ╠═95ce4fe1-553d-439b-9a0e-bafa8e78249c
# ╠═a0363900-088a-4755-a794-1ea081336ba7
# ╟─51ee7ced-4f89-4406-99dc-209b5577e2fc
# ╟─32bec8a9-72ad-438e-b07b-6ff6dfcf5f00
# ╠═f0bb31bf-6dbe-4070-b264-745d8db34761
# ╠═32a6b7b6-c63d-4057-9be2-1dda3f4dac02
# ╟─dbae6e00-e696-48d2-8db5-0ead90a73b7f
# ╠═abd1e2a2-323b-451d-a61d-8e7f10d653f7
# ╠═af7c1cc6-3e70-4402-9f5e-3364cd1aee9b
# ╟─ee5280b0-93ea-4026-bee2-0671609e2cff
# ╠═33105ca3-8784-469d-aef5-779cb999a656
# ╟─631d714f-e096-4a92-92cb-728cd9edd7ef
# ╠═5ff923fe-1956-4d2a-9576-f87d0c15b418
# ╠═f309fd5d-098d-486a-befb-2f6c40402758
# ╟─99e4211e-4db7-4d15-9829-185c107c32a0
# ╟─3347a4a4-8c33-4f57-b4f9-0c2e76523781
# ╟─e4af5e30-7fbb-4309-8c6b-541791cc1736
# ╠═a677ce4b-fff7-49b4-a44d-9a0a9b973c4a
# ╠═fad8fb11-79cd-48e3-bf90-dc7e77935ded
# ╟─497ea09b-9293-493d-910c-622e03213948
# ╟─d592b362-4aa6-4905-a118-3410ebb7b9b1
# ╠═b703e868-7bb3-4a3b-9c08-9515ce19448c
# ╠═445f819e-1b85-40ab-baa0-a4bae62ea571
# ╟─74600acc-6871-4527-824e-5bf0dae8748a
# ╠═b2ca038c-d6f7-4032-8f53-da0d4021c6cc
# ╟─94f6f3e5-adae-478c-84ba-bad33d7abf24
# ╟─43038b33-81dc-45c0-a67f-9c9b75c04ef8
# ╠═a1ffb564-6bee-4535-a9c1-a7574c0739fe
# ╟─9ae65ab6-2fe4-48f3-bf86-6f41c0791c7e
# ╠═aaa0e3b0-66db-4331-a7fe-0faf7c8d5800
# ╟─13a539fb-4637-4da4-a901-ac6dda1358ce
# ╟─c463c322-01ce-4d03-b3c7-31097919eb4b
# ╠═6036b1fc-0086-4e34-88cf-0d6423e418af
# ╟─d7b59463-b2a8-48ad-856c-8c148e19f3c2
# ╠═f890ce1f-a252-4e05-b928-10cb67cc4a04
# ╟─3d83c00c-f7a0-415c-8675-df73a8096ae4
# ╠═f3285fce-3d2f-4a43-84b0-91b433fa22e4
# ╠═61e5718c-39c2-45c6-9cbb-c439fb0a5c67
# ╟─25ea2cee-ba16-4da4-b789-6dcadfa6485c
# ╠═3219def9-6c0d-47a3-8cde-5e708be2bedf
# ╠═356e89b9-c098-4bca-ba08-2bae5634170c
# ╠═37e4163e-34ef-4695-9094-8f272d533448
