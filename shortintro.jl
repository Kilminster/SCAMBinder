### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ d803e312-4cab-11eb-0348-9bd9a0c495a3
using SCAM

# ╔═╡ 70bbb5f2-4cab-11eb-1062-77c360940015
md"""# A short introduction to SCAM

SCAM is a Julia package for doing symbolic computations with an emphasis on algebra and mathematics.
"""

# ╔═╡ dbf23514-4cab-11eb-2128-97a015ba5510
md"""## Symbolic Expressions

The easiest way to introduce a symbolic expression is with the `@symbolic` macro.  For example:
"""

# ╔═╡ f93d31a8-4cab-11eb-1db4-cb6c5a297b17
@symbolic 1+x+5+y+x

# ╔═╡ 02a41cfc-4cac-11eb-2881-fd8ebdffb070
@symbolic ∂{sin(cos(x)^2),x}

# ╔═╡ 1449ef54-4cac-11eb-1c38-f73c035f05cd
@symbolic ∫{x^2*cos(x),x}

# ╔═╡ 3bb7da6a-4cac-11eb-38ca-579700610293
md"""Sometimes it is useful to tag an atom in the expression with a specific type:"""

# ╔═╡ 4e597250-4cac-11eb-17d9-377718ea8989
@symbolic cos(x) x::Real

# ╔═╡ 5fb0cf12-4cac-11eb-05f3-2f25525efa88
md"""(If the type of an atom is not specified, SCAM assigns it the *bottom* type, the elements of which are of *every* type.  The effect of this is that SCAM is free to make any assumptions it likes about the variables represented by such atoms.  Usually, this is a convenience.)

Symbolic expressions can be interpolated into the definitions of other expressions:
"""

# ╔═╡ f905f8d6-4cac-11eb-0602-e9bda90fd4bc
xplusone=@symbolic x+1

# ╔═╡ 78904e8a-4cad-11eb-0760-5f987c43e4ee
@symbolic $(xplusone)^2

# ╔═╡ ac33d6a6-4ccd-11eb-1c03-4babe96d2a76
md"""`SCAM.asexpr` can covert many symbolic expresssions into Julia expressions.

The main exception to this is that SCAM abuses Julia's type parameterisation notation to provide alternative "application-like" symbolic expressions, using the curly rather than round brackets.  (See for example the expressions involving the derivative and integral, above.)

For example, we could interpolate `xplusone` into the definiton of a Julia function:"""

# ╔═╡ 258c3374-4cce-11eb-215f-b3a9b91efb36
@eval plusone(x)=$(SCAM.asexpr(xplusone))

# ╔═╡ 7e87aaa6-4cce-11eb-24df-a3fa20806b2e
plusone(3)

# ╔═╡ 903b2c78-4cce-11eb-2337-0f3a55092318
md"""## Simplification

`SCAM.simplify` will automatically apply a standard set of manipulations to "simplify" a symbolic expression:
"""

# ╔═╡ 040a5432-4ccf-11eb-2cb2-d7c2bf18ddd9
SCAM.simplify(@symbolic 1+x+5+y+x)

# ╔═╡ 2277e5f4-4ccf-11eb-146c-2b48fae610f8
md"""More conveniently, the `@s` macro allows the definition of a symbolic expression, immediately followed by its simplification:"""

# ╔═╡ 552acc28-4ccf-11eb-0229-1dc42e9366e5
@s 1+x+5+y+x

# ╔═╡ 7c51ad02-4cd0-11eb-10a4-b15236b28c56
md"""Most of the computer algebra capabilities of SCAM are available through `SCAM.simplify`.  For example:"""

# ╔═╡ 8bc76bd6-4ccf-11eb-279d-29a409f9f5f7
@s ∂{sin(cos(x)^2),x}

# ╔═╡ a0ee1908-4ccf-11eb-1c3a-a5e9c21009d7
@s ∫{x^2*cos(x),x}

# ╔═╡ 4458c9c0-5bea-11eb-2f78-4119de4b14b4
md"""## Rearranging expressions

`expand` can be used to distribute products and powers over sums:
"""

# ╔═╡ dc49ac24-5d9b-11eb-1f2f-53cbf7902cb6
P=@s 3*(1+a+x+y)*x*(1+a)^2

# ╔═╡ 8b0dc0d2-5bea-11eb-0721-6dae06c93ab6
@s expand($P)

# ╔═╡ 9201ee8a-5beb-11eb-0e34-5d94df5c72b4
md"""The expansion can be limited to a specified set of variables:"""

# ╔═╡ bb9d07d0-5bea-11eb-18ff-c58c999a267e
@s expand{x,y}($P)

# ╔═╡ 381b6860-64ab-11eb-336e-1dcf81dfda6f
md"""Or even some expressions:"""

# ╔═╡ 67a60b12-64ab-11eb-1407-3fe761621f9f
@s expand{sin(x),cos(x)}(∫{x^2*cos(x),x})

# ╔═╡ ba177564-5d9b-11eb-150f-bb044ffb44c6
md"""`mapcoeff{variables...}(expression)(function)` maps a `function` over the coefficients in an `expression` of the specified `variables`.

For example, we can expand an expression in $x$ and $y$, and then independently expand each resulting coefficient:"""

# ╔═╡ 86e988da-5d9b-11eb-372b-a5a4198a264f
@s mapcoeff{x,y}(expand{x,y}($P))(expand)

# ╔═╡ ceaa94f6-4cd0-11eb-33b6-554e1bef22c1
md"""## A little more on curly forms

The main difference between normal applications, `f(x,...)`, and curly forms, `f{x,...}`, is that simplification does not, by default, recurse into the sub-arguments of a curly form.  See for example:
"""

# ╔═╡ 4d289c9c-4cd1-11eb-2d2f-e1b1f98c04c7
@s f(x+x)+f{x+x}

# ╔═╡ b262d122-4cd1-11eb-36c1-9b3261025182
md"""Curly forms are often used to represent manipulations that are not straight-forward simplifications.  For example, `subst` can be used to perform substitution:"""

# ╔═╡ 35a3da66-4cd2-11eb-2900-cd021899f1e5
@s subst{x,z+2}(x^2)

# ╔═╡ 5f8fe63c-4cd2-11eb-2c67-49275c1fa852
md"""And in places where immediate evalution might be undesirable.  For example in λ-expressions:"""

# ╔═╡ 7df84df8-4cd2-11eb-0c5e-415b6d97a25b
doubleplusa=@s λ{a+x+x,x}

# ╔═╡ bf970276-4cd3-11eb-26d0-f580c9d89cf3
@s $(doubleplusa)(3)

# ╔═╡ 154c964c-5a6a-11eb-37ef-8790db38250c
md"""## Context Values

SCAM maintains a "context" with respect to which it interprets expressions.  One aspect of this is the provision of a general dictionary of values.  These can be accessed by using the `contextvalue` curly form:
"""

# ╔═╡ b6d10d86-5a6f-11eb-32d3-3b9d43b70b8f
@s contextvalue{MyContextValue}

# ╔═╡ c4802938-5a6f-11eb-225a-ef62acbdfb35
md"""Context values can be set using the `context` curly form:"""

# ╔═╡ edd13662-5a6f-11eb-1a75-fb935e3bc069
@s context{2*contextvalue{MyContextValue},MyContextValue,3}

# ╔═╡ 5d811142-5a70-11eb-118a-49fa87654827
md"""`context` introduces a sub-context in which the context value is set - the change is not global:"""

# ╔═╡ c5729674-5a70-11eb-3fac-5b810eaa92ab
let A,B
	A=@symbolic contextvalue{a}
	B=@symbolic contextvalue{b}
	
	@s context{f(context{f($A,$B),b,two},f($A,$B)),a,one}
end

# ╔═╡ 38f69a8c-5a6c-11eb-10fc-3b91b03f9695
md"""### Example - integration effort

Eventually, it is planned to implement more sophisticated methods, however for now,
SCAM uses a heuristic method for performing indefinite integration in a similar way that a human might integrate by hand, combining pattern matching known forms with search for appropriate substitutions, or applications of integration by parts.  (Even when more sophisticated methods are implemented, the heuristic method is likely to be tried first.)

The problem is that the search could conceivably continue forever and that eventually the heuristic method will need to "give up".

The following integral can be solved by a substitution, and then a number of applications of integration by parts, but, by default, this is more steps than the heuristic integrator is willing to spend:
"""

# ╔═╡ e6f4e072-5a6b-11eb-184f-2d0a8b9c6429
tricky_integral=@s ∫{sin(x)^6*cos(sin(x))*cos(x),x}

# ╔═╡ 8ff0d364-5a6e-11eb-153e-e9c26d19064a
md"""The effort that the heuristic integrator will spend is given by the `integration_effort` context value:"""

# ╔═╡ 7b54706e-5a6e-11eb-31bc-b71982cb1450
@s contextvalue{integration_effort}

# ╔═╡ b59e8962-5a6e-11eb-0eaf-455deb30d040
md"""We can try the integral again with an increased effort:"""

# ╔═╡ 0db3a414-5a6c-11eb-3c09-cd33270b3e01
@s context{$tricky_integral,integration_effort,200}

# ╔═╡ a56dcbca-7ea4-11eb-1b67-11a0ba5b8dae
md"""### Example - Assumptions

Another context value used by SCAM are the "assumptions", this stores a set of auxilliary facts with respect which SCAM will simplify expressions.  The `assume` form should be used for extending the assumptions:
"""

# ╔═╡ cb609e7a-7ea4-11eb-0cf0-b3ed79c79cfb
@s assume{assume{contextvalue{assumptions},x^2>0,a==b},b==0}

# ╔═╡ bc178806-7ea5-11eb-3268-5783ff871531
md"""SCAM refuses to compute the following integral because it doesn't have enough information about $a$:"""

# ╔═╡ 8ffa7fa8-7ea5-11eb-1ced-dbfe45d881d6
integral_needing_extra_assumptions=@symbolic expand{x}(∫{(x^2+a^2)^(5/2),x});

# ╔═╡ da1067b0-7eaa-11eb-2544-ef2ebbdff763
@s $integral_needing_extra_assumptions

# ╔═╡ cce05ba4-7ea5-11eb-3313-a30764da387d
md"""We can help out by letting SCAM know that we have non-zero values of $a$ in mind:"""

# ╔═╡ 150812e6-7ea6-11eb-3bfe-fb8c676e0d23
@s assume{$integral_needing_extra_assumptions,¬(a==0)}

# ╔═╡ 4184189a-7ea6-11eb-179b-2bdf30477e4c
md"""You might be disappointed that the $\sqrt{a^2}$ terms have not been simplified further, but consider what happens when $a$ is *negative* and compare to the positive case.

If we would like to tell SCAM that, in fact, $a$ is positive, it can simplify further:"""

# ╔═╡ ac72fb70-7ea6-11eb-1700-5dbbe68707c8
@s assume{$integral_needing_extra_assumptions,a>0}

# ╔═╡ Cell order:
# ╟─70bbb5f2-4cab-11eb-1062-77c360940015
# ╠═d803e312-4cab-11eb-0348-9bd9a0c495a3
# ╟─dbf23514-4cab-11eb-2128-97a015ba5510
# ╠═f93d31a8-4cab-11eb-1db4-cb6c5a297b17
# ╠═02a41cfc-4cac-11eb-2881-fd8ebdffb070
# ╠═1449ef54-4cac-11eb-1c38-f73c035f05cd
# ╟─3bb7da6a-4cac-11eb-38ca-579700610293
# ╠═4e597250-4cac-11eb-17d9-377718ea8989
# ╟─5fb0cf12-4cac-11eb-05f3-2f25525efa88
# ╠═f905f8d6-4cac-11eb-0602-e9bda90fd4bc
# ╠═78904e8a-4cad-11eb-0760-5f987c43e4ee
# ╟─ac33d6a6-4ccd-11eb-1c03-4babe96d2a76
# ╠═258c3374-4cce-11eb-215f-b3a9b91efb36
# ╠═7e87aaa6-4cce-11eb-24df-a3fa20806b2e
# ╟─903b2c78-4cce-11eb-2337-0f3a55092318
# ╠═040a5432-4ccf-11eb-2cb2-d7c2bf18ddd9
# ╟─2277e5f4-4ccf-11eb-146c-2b48fae610f8
# ╠═552acc28-4ccf-11eb-0229-1dc42e9366e5
# ╟─7c51ad02-4cd0-11eb-10a4-b15236b28c56
# ╠═8bc76bd6-4ccf-11eb-279d-29a409f9f5f7
# ╠═a0ee1908-4ccf-11eb-1c3a-a5e9c21009d7
# ╟─4458c9c0-5bea-11eb-2f78-4119de4b14b4
# ╠═dc49ac24-5d9b-11eb-1f2f-53cbf7902cb6
# ╠═8b0dc0d2-5bea-11eb-0721-6dae06c93ab6
# ╟─9201ee8a-5beb-11eb-0e34-5d94df5c72b4
# ╠═bb9d07d0-5bea-11eb-18ff-c58c999a267e
# ╟─381b6860-64ab-11eb-336e-1dcf81dfda6f
# ╠═67a60b12-64ab-11eb-1407-3fe761621f9f
# ╟─ba177564-5d9b-11eb-150f-bb044ffb44c6
# ╠═86e988da-5d9b-11eb-372b-a5a4198a264f
# ╟─ceaa94f6-4cd0-11eb-33b6-554e1bef22c1
# ╠═4d289c9c-4cd1-11eb-2d2f-e1b1f98c04c7
# ╟─b262d122-4cd1-11eb-36c1-9b3261025182
# ╠═35a3da66-4cd2-11eb-2900-cd021899f1e5
# ╟─5f8fe63c-4cd2-11eb-2c67-49275c1fa852
# ╠═7df84df8-4cd2-11eb-0c5e-415b6d97a25b
# ╠═bf970276-4cd3-11eb-26d0-f580c9d89cf3
# ╟─154c964c-5a6a-11eb-37ef-8790db38250c
# ╠═b6d10d86-5a6f-11eb-32d3-3b9d43b70b8f
# ╟─c4802938-5a6f-11eb-225a-ef62acbdfb35
# ╠═edd13662-5a6f-11eb-1a75-fb935e3bc069
# ╟─5d811142-5a70-11eb-118a-49fa87654827
# ╠═c5729674-5a70-11eb-3fac-5b810eaa92ab
# ╟─38f69a8c-5a6c-11eb-10fc-3b91b03f9695
# ╠═e6f4e072-5a6b-11eb-184f-2d0a8b9c6429
# ╟─8ff0d364-5a6e-11eb-153e-e9c26d19064a
# ╠═7b54706e-5a6e-11eb-31bc-b71982cb1450
# ╟─b59e8962-5a6e-11eb-0eaf-455deb30d040
# ╠═0db3a414-5a6c-11eb-3c09-cd33270b3e01
# ╟─a56dcbca-7ea4-11eb-1b67-11a0ba5b8dae
# ╠═cb609e7a-7ea4-11eb-0cf0-b3ed79c79cfb
# ╟─bc178806-7ea5-11eb-3268-5783ff871531
# ╠═8ffa7fa8-7ea5-11eb-1ced-dbfe45d881d6
# ╠═da1067b0-7eaa-11eb-2544-ef2ebbdff763
# ╟─cce05ba4-7ea5-11eb-3313-a30764da387d
# ╠═150812e6-7ea6-11eb-3bfe-fb8c676e0d23
# ╟─4184189a-7ea6-11eb-179b-2bdf30477e4c
# ╠═ac72fb70-7ea6-11eb-1700-5dbbe68707c8
