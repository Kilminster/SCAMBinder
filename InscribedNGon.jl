### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : missing
        el
    end
end

# ╔═╡ cf7ef3a8-a528-11eb-1b17-03a8aa48504c
begin
	using PlutoUI
	using SCAM
	using Plots
	
	md"""# A roundabout way to find the area of an n-gon inscribed in the unit circle"""
end

# ╔═╡ 4bb099d3-fbe2-4931-a93d-2cb9d571a3ae
md"""The following function of $x$ is the upper unit semicircle."""

# ╔═╡ fd1ad592-28a4-444b-b9f0-9cf10ce778a9
uppercircle=@s sqrt(1-x^2)

# ╔═╡ 8cc1329c-734f-400d-88b0-1fdcc987d209
@eval plot(x->$(SCAM.asexpr(uppercircle)),xlims=(-1,1),label=SCAM.LaTeX.aslatex(uppercircle),aspectratio=1,color=:blue)

# ╔═╡ 2620d382-14dc-443f-9b3a-9aa348aee9c4
md"""So integrating between -1 and 1 and doubling gives the area of the circle:"""

# ╔═╡ c29222ef-d90f-452e-8403-708cc919c4cd
areacircle=@s 2*∫{$uppercircle,x,-1,1}

# ╔═╡ c0787f07-e3ee-437d-8d02-116025a0d1aa
md"""Now consider the n-gon inscribed in the circle. (You can choose a value for n using the slider.)"""

# ╔═╡ 84a5a835-2f9b-41dd-8d23-4dfe1d5322e1
@bind N Slider(3:12,show_value=true)

# ╔═╡ 7fb7aecb-627a-4e2c-af08-410e6404806e
md"""The position of the rightmost edge of the n-gon is given by $x=x_0$ where"""

# ╔═╡ 3f84b909-af89-4ba4-9bea-70e73294b13f
x0=@s cos(pi/n)

# ╔═╡ d22c85fc-ace9-4caf-b4e3-2e5039564c64
md"""Or, specifically:"""

# ╔═╡ cb82397b-6143-426d-8d09-97f4433ffb8a
x0_specific=@s subst{n,$N}($x0)

# ╔═╡ caa8082c-6841-4098-b2d9-98c7520258fd
begin
	plot(x->sqrt(1-x^2),xlims=(-1,1),label="",color=:blue)
	plot!(x->-sqrt(1-x^2),xlims=(-1,1),label="",aspectratio=1,color=:blue)
	plot!([cos((i+1/2)/N*2*pi) for i=0:N],[sin((i+1/2)/N*2*pi) for i=0:N],color=:red,label="")
	x0_eval=eval(SCAM.asexpr(x0_specific))
	plot!([x0_eval,x0_eval],[-1,1],label="x0",color=:green)
end

# ╔═╡ 79f6a075-4d0a-4017-aca3-55d679eed323
md"""So the area of the blue circle cut off by the green line in the above diagram is just:"""

# ╔═╡ c2183e77-880b-4b63-9ff4-b85bbf431eaf
cut_off_area=@s 2*∫{sqrt(1-x^2),x,$x0,1}

# ╔═╡ 98ac8270-80eb-4a80-a4c8-720b8eb2888b
md"""The total area we need to remove is just $n$ times this, so the total area of the inscribed n-gon is:"""

# ╔═╡ cb3e5380-3682-43ee-84ce-9291b12f075d
areangon=@s expand($(areacircle)-n*$(cut_off_area)) # and expand to simplify a little.

# ╔═╡ 6e20bdef-7c82-4b89-a9ac-4800227ee28b
md"""Substituting our specific value of n gives"""

# ╔═╡ 0ffee7ef-33ec-4a94-8246-19a753142f8c
specific_area_ngon=@s expand(subst{n,$N}($areangon))

# ╔═╡ ac63be12-376a-4b77-a75b-d58ba131a8fe
eval(SCAM.asexpr(specific_area_ngon))

# ╔═╡ 5fee3816-94f3-4b03-a958-fe265fa80be7
md"""## A saner way to do the calculation

The more sensible way to do the above calculation, of course, is to consider the n-gon as made up of n triangles, and find the base and height of these triangles.

The height, we know is just x0, above.
"""

# ╔═╡ cd836b4c-25d5-42c9-8b4c-3706d8338a19
height=x0

# ╔═╡ 041d2d01-4679-455f-b386-17196d24b5bc
md"""The base can be computed using Pythagoras:"""

# ╔═╡ 9870f6e3-8e13-4654-9394-68cd1bf131df
base=@s 2*sqrt(1-($height)^2)

# ╔═╡ 221ed476-ccb6-4e41-8e98-8c03866f174b
area_triangle=@s 1/2*$base*$height

# ╔═╡ bdb45981-1401-458b-8768-455a4b2452b4
sane_area_ngon=@s n*$area_triangle

# ╔═╡ a02fde4f-30b9-4011-86bb-781e7c1a9108
md"""Compare the two methods of computation:"""

# ╔═╡ cc236a46-cb74-4fd4-bcfc-de81a8d1b4cc
difference=@s $areangon-$sane_area_ngon

# ╔═╡ c760c382-5566-48e3-9395-d7a97c7f6066
md"""This quantity should vanish, of course, but SCAM is being careful.  It doesn't know that n is the number of sides of a polygon (ie an integer and at least 3).  Let's tell it that:"""

# ╔═╡ b81b5690-e1c5-4743-8258-38b7f35112c2
@s assume{$difference,n>=3,isInteger(n)}

# ╔═╡ 4b91e221-44f1-4d76-b3ba-ad52f8711e31
md"""This helps a bit, but we have reached the limit of what SCAM currently knows.  Ideally, as shown by the graph below, it would be able to prove that 0<π/n<π and so asin(cos(π/n))=π/2-π/n.
"""

# ╔═╡ 2f99277c-6eb0-43e1-86ef-62b27ea8f7cf
plot(x->asin(cos(x)),xlims=(-5,5),label="asin(cos(x))")

# ╔═╡ a0896a77-f47d-4f73-bcff-97879ce2e9f2
md"""But we can make the appropriate substitution by hand:"""

# ╔═╡ 160a6a85-cb89-45e1-844a-feb27dd8d858
@s assume{expand(subst{asin(cos(n^(-1)*π)),π/2-π/n}($difference)),n>=3,isInteger(n)}

# ╔═╡ Cell order:
# ╟─cf7ef3a8-a528-11eb-1b17-03a8aa48504c
# ╟─4bb099d3-fbe2-4931-a93d-2cb9d571a3ae
# ╠═fd1ad592-28a4-444b-b9f0-9cf10ce778a9
# ╠═8cc1329c-734f-400d-88b0-1fdcc987d209
# ╟─2620d382-14dc-443f-9b3a-9aa348aee9c4
# ╠═c29222ef-d90f-452e-8403-708cc919c4cd
# ╟─c0787f07-e3ee-437d-8d02-116025a0d1aa
# ╠═84a5a835-2f9b-41dd-8d23-4dfe1d5322e1
# ╟─caa8082c-6841-4098-b2d9-98c7520258fd
# ╟─7fb7aecb-627a-4e2c-af08-410e6404806e
# ╠═3f84b909-af89-4ba4-9bea-70e73294b13f
# ╟─d22c85fc-ace9-4caf-b4e3-2e5039564c64
# ╠═cb82397b-6143-426d-8d09-97f4433ffb8a
# ╟─79f6a075-4d0a-4017-aca3-55d679eed323
# ╠═c2183e77-880b-4b63-9ff4-b85bbf431eaf
# ╟─98ac8270-80eb-4a80-a4c8-720b8eb2888b
# ╠═cb3e5380-3682-43ee-84ce-9291b12f075d
# ╟─6e20bdef-7c82-4b89-a9ac-4800227ee28b
# ╠═0ffee7ef-33ec-4a94-8246-19a753142f8c
# ╠═ac63be12-376a-4b77-a75b-d58ba131a8fe
# ╟─5fee3816-94f3-4b03-a958-fe265fa80be7
# ╠═cd836b4c-25d5-42c9-8b4c-3706d8338a19
# ╟─041d2d01-4679-455f-b386-17196d24b5bc
# ╠═9870f6e3-8e13-4654-9394-68cd1bf131df
# ╠═221ed476-ccb6-4e41-8e98-8c03866f174b
# ╠═bdb45981-1401-458b-8768-455a4b2452b4
# ╟─a02fde4f-30b9-4011-86bb-781e7c1a9108
# ╠═cc236a46-cb74-4fd4-bcfc-de81a8d1b4cc
# ╟─c760c382-5566-48e3-9395-d7a97c7f6066
# ╠═b81b5690-e1c5-4743-8258-38b7f35112c2
# ╟─4b91e221-44f1-4d76-b3ba-ad52f8711e31
# ╠═2f99277c-6eb0-43e1-86ef-62b27ea8f7cf
# ╟─a0896a77-f47d-4f73-bcff-97879ce2e9f2
# ╠═160a6a85-cb89-45e1-844a-feb27dd8d858
