### A Pluto.jl notebook ###
# v0.14.4

using Markdown
using InteractiveUtils

# ╔═╡ 4397061d-99d8-4ab7-baf6-1e24819e0be6
using SCAM

# ╔═╡ 85a1092c-aa7f-11eb-1381-c9e4429f461f
md"""### SCAM Demo

Welcome to a demo SCAM environment hosted on [mybinder.org](https://mybinder.org/).

SCAM is a Julia package for doing symbolic computations with an emphasis on algebra and mathematics.  

You can start using SCAM immediately in this notebook, or else load one of the following examples.

* [shortintro.jl](open?path=%2Fhome%2Fjovyan%2Fshortintro.jl) -- A short introduction to SCAM.
* [reference.jl](open?path=%2Fhome%2Fjovyan%2Freference.jl) -- SCAM's reference manual.
* [InscribedNGon.jl](open?path=%2Fhome%2Fjovyan%2FInscribedNGon.jl) -- A roundabout way to calculated the area of an inscribed n-gon.

You are currently using the [Pluto.jl](https://github.com/fonsp/Pluto.jl) notebook environment.  If you would prefer to use SCAM in a [Jupyter](https://jupyter.org/) environment, that is also [available](../tree).

"""

# ╔═╡ ab179434-e323-46ec-a988-1e9db7648d2e


# ╔═╡ Cell order:
# ╟─85a1092c-aa7f-11eb-1381-c9e4429f461f
# ╠═4397061d-99d8-4ab7-baf6-1e24819e0be6
# ╠═ab179434-e323-46ec-a988-1e9db7648d2e
